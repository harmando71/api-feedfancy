import requests
import validators
from django.template.defaultfilters import slugify
from rest_framework import serializers
from rest_framework.exceptions import MethodNotAllowed
from bs4 import BeautifulSoup
from core.models import SourceUrl, Post, Taxonomy, Source


class PublicTaxonomySerializer(serializers.ModelSerializer):
    class Meta:
        model = Taxonomy
        fields = ['slug', 'name']


class PublicSourceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Source
        fields = ['name', 'url']


class PublicPostSerializer(serializers.ModelSerializer):
    taxonomies = PublicTaxonomySerializer(many=True, read_only=True)
    source = PublicSourceSerializer(many=False, read_only=True)

    class Meta:
        model = Post
        fields = ['id', 'slug', 'title', 'content', 'summary', 'link', 'imgUrl', 'taxonomies', 'source']

    def create(self, validated_data):
        raise MethodNotAllowed('Solicitud no valida')

    def update(self, instance, validated_data):
        raise MethodNotAllowed('Solicitud no valida')


class SpiderSerializer(serializers.Serializer):

    def to_representation(self, instance):
        response = []
        # shows only where isActive is True
        uri = SourceUrl.objects.filter(taxonomies__slug='politica', isActive=True).order_by('?')
        for u in uri:
            r = requests.get(u.url)
            if r.status_code != 200:
                continue
            soup = BeautifulSoup(r.text, 'html.parser')
            # get data by class main from source url
            links = soup.find_all(True, {'class': [u.metadata['parent']]}, limit=u.metadata['size'], recursive=True)
            # iter result find
            for l in links:
                post = {}
                # Apply rules for source url
                if not u.metadata:
                    continue
                for key, value in u.metadata.items():
                    # Exclude rules parent and size
                    if key != 'parent' and key != 'size':
                        # Extracting data by rules and search
                        for a in l.find_all(value[0]):
                            # Build post by rules keys
                            if value[1] == 'get':
                                post[key] = a.get(value[2])
                            elif value[1] == 'text':
                                post[key] = a.text
                response.append(post)
                if post['link'].startswith('http'):
                    if requests.get(post['link']).status_code != 200:
                        continue
                img_default = 'https://dummyimage.com/600x400/ede6ed/1b1c1f.png&text=FeedFancy' #url por default en caso de que no exista imagen
                if validators.url(post['img']): #aportacion de Brenda solo instalar validators con pip install validators en importarlo con import validators
                    if requests.get(post['img']).status_code == 200: #checa si el archivo esta disponible en ua url
                        img_default = post['img'] #asigna la url correcta y su archivo disponible
                try:
                    post = Post.objects.create(
                        title=self.string(post['title']), #elimina espacios en los titulos
                        link=post['link'],
                        imgUrl=img_default, #asignacion de la imagen por default al modelo post
                        summary=self.string(post['summary']), #elimina espacios en el summary
                        slug=slugify(post['title']),
                        source=u.source
                    )
                    for t in u.taxonomies.all():
                        post.taxonomies.add(t)
                except:
                    continue
        return response

    def create(self, validated_data):
        raise MethodNotAllowed('Solicitud no permitida')

    def update(self, instance, validated_data):
        raise MethodNotAllowed('Solicitud no permitida')

    #método que elimina los espacios y separa el title de summary
    def string(self, strn): #recibe la cadena
        strn = strn.strip() #elimina los espacion antes y despues del ultimo caracter
        x = strn.find(':') #busca los dos puntos retornando la posicion donde lo encontro
        if int(x) == 2: #si es la segunda posicion indica tiene que eliminar la hora
            strn = strn[5:] #corta la hora
        elif int(x) != -1: #si no llego al final de la cadena
            strn = strn[0:int(x)-2] #corta de los dos puntos hacia atras
        strn = strn.strip()
        return strn